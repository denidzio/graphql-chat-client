import React from "react";
import Chat from "./chat/Chat";

function App() {
  return (
    <div className="app">
      <Chat />
    </div>
  );
}

export default App;
