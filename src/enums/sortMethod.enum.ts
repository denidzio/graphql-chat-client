enum SORT_METHOD {
  LIKES_DESC = "likes_DESC",
  LIKES_ASC = "likes_ASC",
  DISLIKES_DESC = "dislikes_DESC",
  DISLIKES_ASC = "dislikes_ASC",
  CREATED_AT_DESC = "createdAt_DESC",
  CREATED_AT_ASC = "createdAt_ASC",
}

export default SORT_METHOD;
