import IReply from "./IReply";

interface IMessage {
  id: string;
  text?: string;
  likes?: number;
  dislikes?: number;
  replies?: IReply[];
}

export default IMessage;
