import { SORT_METHOD } from "../enums";

interface IMessagesVars {
  filter?: string;
  orderBy?: SORT_METHOD;
  skip?: number;
  first?: number;
}

export default IMessagesVars;
