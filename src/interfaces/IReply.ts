interface IReply {
  id: string;
  text?: string;
  likes?: number;
  dislikes?: number;
}

export default IReply;
