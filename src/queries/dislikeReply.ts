import { gql } from "@apollo/client";

const DISLIKE_REPLY = gql`
  mutation dislikeReplyQuery($id: ID!) {
    dislikeReply(id: $id) {
      id
      dislikes
    }
  }
`;

export default DISLIKE_REPLY;
