import { gql } from "@apollo/client";

const NEW_REPLY_SUBSCRIPTION = gql`
  subscription OnReplyMessage {
    newReply {
      id
      text
      likes
      dislikes
      message {
        id
      }
    }
  }
`;

export default NEW_REPLY_SUBSCRIPTION;
