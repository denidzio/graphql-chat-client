import { gql } from "@apollo/client";

const UPDATE_MESSAGE_SUBSCRIPTION = gql`
  subscription OnMessageUpdate {
    updateMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export default UPDATE_MESSAGE_SUBSCRIPTION;
