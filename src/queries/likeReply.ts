import { gql } from "@apollo/client";

const LIKE_REPLY = gql`
  mutation likeReplyQuery($id: ID!) {
    likeReply(id: $id) {
      id
      likes
    }
  }
`;

export default LIKE_REPLY;
