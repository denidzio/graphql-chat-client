import { gql } from "@apollo/client";

const SEND_MESSAGE = gql`
  mutation sendMessageQuery($text: String!) {
    sendMessage(text: $text) {
      id
      text
    }
  }
`;

export default SEND_MESSAGE;
