import { gql } from "@apollo/client";

const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription OnMessageSent {
    newMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export default NEW_MESSAGES_SUBSCRIPTION;
