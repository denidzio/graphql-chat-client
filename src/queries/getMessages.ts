import { gql } from "@apollo/client";

const GET_MESSAGES = gql`
  query getMessagesQuery(
    $filter: String
    $orderBy: MessageOrderByInput
    $skip: Int
    $first: Int
  ) {
    messages(filter: $filter, orderBy: $orderBy, skip: $skip, first: $first) {
      list {
        id
        text
        likes
        dislikes
        replies {
          id
          text
          likes
          dislikes
        }
      }
      count
    }
  }
`;

export default GET_MESSAGES;
