import { gql } from "@apollo/client";

const UPDATE_REPLY_SUBSCRIPTION = gql`
  subscription OnReplyUpdate {
    updateReply {
      id
      text
      likes
      dislikes
      message {
        id
      }
    }
  }
`;

export default UPDATE_REPLY_SUBSCRIPTION;
