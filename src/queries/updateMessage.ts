import { gql } from "@apollo/client";

const UPDATE_MESSAGE = gql`
  query UpdateMessageQuery($id: ID!) {
    message(id: $id) {
      id
      text
      likes
      dislikes
      replies
    }
  }
`;

export default UPDATE_MESSAGE;
