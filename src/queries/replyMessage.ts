import { gql } from "@apollo/client";

const REPLY_MESSAGE = gql`
  mutation replyMessageQuery($messageId: ID!, $text: String!) {
    replyMessage(messageId: $messageId, text: $text) {
      id
      text
    }
  }
`;

export default REPLY_MESSAGE;
