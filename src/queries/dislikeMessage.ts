import { gql } from "@apollo/client";

const DISLIKE_MESSAGE = gql`
  mutation dislikeMessageQuery($id: ID!) {
    dislikeMessage(id: $id) {
      id
      dislikes
    }
  }
`;

export default DISLIKE_MESSAGE;
