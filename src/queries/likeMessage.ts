import { gql } from "@apollo/client";

const LIKE_MESSAGE = gql`
  mutation likeMessageQuery($id: ID!) {
    likeMessage(id: $id) {
      id
      likes
    }
  }
`;

export default LIKE_MESSAGE;
