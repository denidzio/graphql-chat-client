export const isDisableNext = (
  itemsCount: number,
  currentPage: number,
  itemsPerPage: number
) => {
  if (currentPage * itemsPerPage < itemsCount) {
    return false;
  }

  return true;
};

export const isDisablePrev = (currentPage: number) => {
  if (currentPage !== 1) {
    return false;
  }

  return true;
};
