export const getMessageNumber = (id: string) => {
  return id.slice(-3);
};
