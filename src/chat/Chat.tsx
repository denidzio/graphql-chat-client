import React, { useState, useCallback } from "react";
import { useQuery } from "@apollo/client";
import { IMessage, IMessagesVars, IReply } from "../interfaces";
import {
  GET_MESSAGES,
  NEW_MESSAGES_SUBSCRIPTION,
  NEW_REPLY_SUBSCRIPTION,
  UPDATE_MESSAGE_SUBSCRIPTION,
  UPDATE_REPLY_SUBSCRIPTION,
} from "../queries";
import { MessageForm, MessageList, Navigation, SortList } from "./components";
import { SORT_METHOD } from "../enums";
import { MESSAGES_PER_PAGE } from "../config";

const defaultVariables: IMessagesVars = {
  skip: 0,
  first: MESSAGES_PER_PAGE,
};

function Chat() {
  const [currentPage, setCurrentPage] = useState(1);

  const { subscribeToMore, ...result } = useQuery(GET_MESSAGES, {
    variables: { ...defaultVariables },
  });

  const handleRefetchBySort = useCallback(
    (method: SORT_METHOD) => {
      result.refetch({ orderBy: method, skip: 0 });
      setCurrentPage(1);
    },
    [result]
  );

  const handleRefetchByFilter = useCallback(
    (text: string) => {
      result.refetch({ filter: text, skip: 0 });
      setCurrentPage(1);
    },
    [result]
  );

  const handleRefetchPagination = useCallback(
    (skip: number) => {
      result.refetch({ skip });
    },
    [result]
  );

  return (
    <div className="chat app__chat">
      <fieldset className="chat-wrapper">
        <legend>GraphQL Chat</legend>
        {!result.loading && !result.error ? (
          <header className="chat-header">
            <Navigation
              currentPage={currentPage}
              messagesCount={result.data.messages.count}
              onPaginate={handleRefetchPagination}
              setCurrentPage={setCurrentPage}
            />
            <SortList onRefetch={handleRefetchBySort} />
          </header>
        ) : null}
        <MessageList
          {...result}
          subscribeToNewMessages={() =>
            subscribeToMore({
              document: NEW_MESSAGES_SUBSCRIPTION,
              updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) {
                  return prev;
                }

                const newMessage = subscriptionData.data.newMessage;

                return {
                  messages: {
                    count: prev.messages.count + 1,
                    list: [...prev.messages.list, newMessage],
                  },
                };
              },
            })
          }
          subscribeToUpdateMessage={() =>
            subscribeToMore({
              document: UPDATE_MESSAGE_SUBSCRIPTION,
              updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) {
                  return prev;
                }

                const messages = prev.messages.list as IMessage[];

                const updatedMessage = subscriptionData.data
                  .updateMessage as IMessage;

                const updatedMessageIndex = messages.findIndex(
                  (message) => message.id === updatedMessage.id
                );

                return {
                  messages: {
                    ...prev.messages,
                    list: messages
                      .slice(0, updatedMessageIndex)
                      .concat(
                        updatedMessage,
                        messages.slice(updatedMessageIndex + 1)
                      ),
                  },
                };
              },
            })
          }
          subscribeToNewReplies={() =>
            subscribeToMore({
              document: NEW_REPLY_SUBSCRIPTION,
              updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) {
                  return prev;
                }

                const messages = prev.messages.list as IMessage[];
                const newReply = subscriptionData.data.newReply;

                const messageToUpdateIndex = messages.findIndex(
                  (message) => message.id === newReply.message.id
                );

                const messageToUpdate = messages[messageToUpdateIndex];

                const updatedMessage = {
                  ...messageToUpdate,
                  replies: [...(messageToUpdate.replies as IReply[]), newReply],
                };

                return {
                  messages: {
                    ...prev.messages,
                    list: messages
                      .slice(0, messageToUpdateIndex)
                      .concat(
                        updatedMessage,
                        messages.slice(messageToUpdateIndex + 1)
                      ),
                  },
                };
              },
            })
          }
          subscribeToUpdateReplies={() =>
            subscribeToMore({
              document: UPDATE_REPLY_SUBSCRIPTION,
              updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) {
                  return prev;
                }

                const messages = prev.messages.list as IMessage[];
                const updatedReply = subscriptionData.data.updateReply;

                const messageToUpdateIndex = messages.findIndex(
                  (message) => message.id === updatedReply.message.id
                );

                const messageToUpdate = messages[messageToUpdateIndex];

                if (!messageToUpdate.replies) {
                  return prev;
                }

                const updatedReplyIndex = messageToUpdate.replies.findIndex(
                  (reply) => reply.id === updatedReply.id
                );

                const updatedMessage = {
                  ...messageToUpdate,
                  replies: messageToUpdate.replies
                    .slice(0, updatedReplyIndex)
                    .concat(
                      updatedReply,
                      messageToUpdate.replies.slice(updatedReplyIndex + 1)
                    ),
                };

                return {
                  messages: {
                    ...prev.messages,
                    list: messages
                      .slice(0, messageToUpdateIndex)
                      .concat(
                        updatedMessage,
                        messages.slice(messageToUpdateIndex + 1)
                      ),
                  },
                };
              },
            })
          }
        />
        {!result.loading && !result.error ? (
          <MessageForm onSearch={handleRefetchByFilter} />
        ) : null}
      </fieldset>
    </div>
  );
}

export default Chat;
