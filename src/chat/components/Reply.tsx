import React, { useCallback } from "react";
import { useMutation } from "@apollo/client";
import { getMessageNumber } from "../../helpers/message.helper";
import { IReply } from "../../interfaces";
import LIKE_REPLY from "../../queries/likeReply";
import DISLIKE_REPLY from "../../queries/dislikeReply";

function Reply({ reply }: { reply: IReply }) {
  const [likeReply] = useMutation(LIKE_REPLY);
  const [dislikeReply] = useMutation(DISLIKE_REPLY);

  const handleLikeReply = useCallback(() => {
    likeReply({ variables: { id: reply.id } });
  }, [reply, likeReply]);

  const handleDislikeReply = useCallback(() => {
    dislikeReply({ variables: { id: reply.id } });
  }, [reply, dislikeReply]);

  return (
    <fieldset className="reply message-replies__reply">
      <legend>{getMessageNumber(reply.id)}</legend>
      <div className="message-content">
        <p className="reply-text">{reply.text}</p>
        <div className="message-action message__actions">
          <div
            className="message-action_like pointer"
            onClick={handleLikeReply}
          >
            +{reply.likes}
          </div>
          <div
            className="message-action_dislike pointer"
            onClick={handleDislikeReply}
          >
            -{reply.dislikes}
          </div>
        </div>
      </div>
    </fieldset>
  );
}

export default Reply;
