import React, {
  useState,
  useCallback,
  ChangeEvent,
  SyntheticEvent,
} from "react";
import { useMutation } from "@apollo/client";
import { REPLY_MESSAGE } from "../../queries";

function ReplyForm(props: { id: string; onClose: () => void }) {
  const { id, onClose } = props;
  const [reply, setReply] = useState("");
  const [replyMessage] = useMutation(REPLY_MESSAGE);

  const handleInputReply = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setReply(e.currentTarget.value);
  }, []);

  const handleReplyMessage = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault();

      if (!reply.trim()) {
        return;
      }

      replyMessage({ variables: { messageId: id, text: reply } });
      onClose();
      setReply("");
    },
    [id, onClose, reply, replyMessage]
  );

  return (
    <form
      action=""
      className="reply-form message-form"
      onSubmit={handleReplyMessage}
    >
      <fieldset className="reply-form__wrapper">
        <input
          name=""
          id=""
          className="reply-form-input"
          onChange={handleInputReply}
        />
        <button
          className="message-form-button message-form__button pointer"
          type="submit"
        >
          Send
        </button>
        <button
          className="message-form-button message-form__button pointer"
          onClick={onClose}
        >
          Close
        </button>
      </fieldset>
    </form>
  );
}

export default ReplyForm;
