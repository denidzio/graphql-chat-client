import React, { useState, useCallback } from "react";
import { SORT_METHOD } from "../../enums";

const sortMethods = [
  {
    method: SORT_METHOD.LIKES_DESC,
    name: "desc by likes",
  },
  {
    method: SORT_METHOD.LIKES_ASC,
    name: "asc by likes",
  },
  {
    method: SORT_METHOD.DISLIKES_DESC,
    name: "desc by dislikes",
  },
  {
    method: SORT_METHOD.DISLIKES_ASC,
    name: "asc by dislikes",
  },
  {
    method: SORT_METHOD.CREATED_AT_DESC,
    name: "desc by date",
  },
  {
    method: SORT_METHOD.CREATED_AT_ASC,
    name: "asc by date",
  },
];

function SortList({ onRefetch = (method: SORT_METHOD) => {} }) {
  const [counter, setCounter] = useState(0);

  const handleChangeCounter = useCallback(() => {
    if (counter < sortMethods.length - 1) {
      setCounter(counter + 1);
      return;
    }

    setCounter(0);
  }, [counter]);

  return (
    <div className="chat-sort">
      Sort{" "}
      <span className="pointer underline" onClick={handleChangeCounter}>
        {sortMethods[counter].name}
      </span>
      <button
        className="chat-sort-button chat-sort__button"
        onClick={() => onRefetch(sortMethods[counter].method)}
      >
        Refetch
      </button>
    </div>
  );
}

export default SortList;
