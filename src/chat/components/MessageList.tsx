import React, { useEffect } from "react";
import { MESSAGES_PER_PAGE } from "../../config";
import { IMessage } from "../../interfaces";
import Message from "./Message";

function MessageList(props: any) {
  const {
    data,
    loading,
    subscribeToNewMessages,
    subscribeToUpdateMessage,
    subscribeToNewReplies,
    subscribeToUpdateReplies,
  } = props;

  useEffect(() => {
    subscribeToNewMessages();
    subscribeToUpdateMessage();
    subscribeToNewReplies();
    subscribeToUpdateReplies();
  }, []);

  if (loading) {
    return <p className="message-list_loading">Loading...</p>;
  }

  return (
    <ul className="message-list">
      {data.messages.list
        .slice(0, MESSAGES_PER_PAGE)
        .map((message: IMessage) => (
          <Message message={message} key={message.id} />
        ))}
    </ul>
  );
}

export default MessageList;
