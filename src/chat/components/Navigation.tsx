import React, { useCallback } from "react";
import { MESSAGES_PER_PAGE } from "../../config";
import { isDisableNext, isDisablePrev } from "../../helpers/navigation.helper";

function Navigation({
  currentPage = 1,
  messagesCount = 0,
  onPaginate = (skip: number) => {},
  setCurrentPage = (page: number) => {},
}) {
  const handleNextPage = useCallback(() => {
    onPaginate(currentPage * MESSAGES_PER_PAGE);
    setCurrentPage(currentPage + 1);
  }, [currentPage, setCurrentPage, onPaginate]);

  const handlePrevPage = useCallback(() => {
    onPaginate(MESSAGES_PER_PAGE * (currentPage - 2));
    setCurrentPage(currentPage - 1);
  }, [currentPage, setCurrentPage, onPaginate]);

  return (
    <div className="chat-navigation">
      <button
        className="chat-navigation-button chat-navigation_prev"
        disabled={isDisablePrev(currentPage)}
        onClick={handlePrevPage}
      >
        Prev
      </button>
      <button
        className="chat-navigation-button chat-navigation_next"
        disabled={isDisableNext(messagesCount, currentPage, MESSAGES_PER_PAGE)}
        onClick={handleNextPage}
      >
        Next
      </button>
    </div>
  );
}

export default Navigation;
