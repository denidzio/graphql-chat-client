import React, { useState, useCallback } from "react";
import { useMutation } from "@apollo/client";
import { Reply, ReplyForm } from ".";
import { getMessageNumber } from "../../helpers/message.helper";
import { IMessage } from "../../interfaces";
import { DISLIKE_MESSAGE, LIKE_MESSAGE } from "../../queries";

function Message({ message }: { message: IMessage }) {
  const [reply, setReply] = useState(false);

  const [likeMessage] = useMutation(LIKE_MESSAGE);
  const [dislikeMessage] = useMutation(DISLIKE_MESSAGE);

  const handleLikeMessage = useCallback(() => {
    likeMessage({ variables: { id: message.id } });
  }, [message, likeMessage]);

  const handleDislikeMessage = useCallback(() => {
    dislikeMessage({ variables: { id: message.id } });
  }, [message, dislikeMessage]);

  return (
    <li className="message-list__item">
      <fieldset className="message">
        <legend>{getMessageNumber(message.id)}</legend>
        <div className="message-content">
          <p className="message-text">{message.text}</p>
          <div className="message-actions message__actions">
            <div className="message-action">
              <div
                className="message-action_like pointer"
                onClick={handleLikeMessage}
              >
                +{message.likes}
              </div>
              <div
                className="message-action_dislike pointer"
                onClick={handleDislikeMessage}
              >
                -{message.dislikes}
              </div>
            </div>
            <div className="message-action_reply">
              {reply ? (
                <ReplyForm id={message.id} onClose={() => setReply(false)} />
              ) : (
                <span className="pointer" onClick={() => setReply(true)}>
                  Reply
                </span>
              )}
            </div>
          </div>
          <div className="message-replies">
            {message.replies?.map((reply) => (
              <Reply reply={reply} key={reply.id} />
            ))}
          </div>
        </div>
      </fieldset>
    </li>
  );
}

export default Message;
