import React, {
  useState,
  useCallback,
  ChangeEvent,
  SyntheticEvent,
} from "react";
import { gql, useMutation } from "@apollo/client";
import { SEND_MESSAGE } from "../../queries";

function MessageForm({ onSearch = (text: string) => {} }) {
  const [message, setMessage] = useState("");

  const [sendMessage] = useMutation(SEND_MESSAGE);

  const handleInputMessage = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setMessage(e.currentTarget.value);
  }, []);

  const handleSendMessage = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault();

      if (!message.trim()) {
        return;
      }

      sendMessage({ variables: { text: message } });
      setMessage("");
    },
    [message, sendMessage]
  );

  const handleSearchMessage = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault();
      onSearch(message);
      setMessage("");
    },
    [message, onSearch]
  );

  return (
    <form
      className="message-form chat__message-form"
      onSubmit={handleSendMessage}
    >
      <fieldset className="message-form__wrapper">
        <label htmlFor="input-message" className="message-form-label">
          <input
            id="input-message"
            className="message-form-input"
            value={message}
            onChange={handleInputMessage}
          />
        </label>
        <button
          className="message-form-button message-form__button pointer"
          type="submit"
        >
          Send
        </button>
        <button
          onClick={handleSearchMessage}
          className="message-form-button message-form__button pointer"
        >
          Search
        </button>
      </fieldset>
    </form>
  );
}

export default MessageForm;
